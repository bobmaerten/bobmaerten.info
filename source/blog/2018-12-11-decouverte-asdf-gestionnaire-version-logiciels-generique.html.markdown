---

title: Découverte de ASDF, un gestionnaire de version de logiciels générique
date: 2018-12-11 07:51 UTC
tags: dev, devops, ruby, postgresql, redis
cover: book.jpg
---

La gestion des dépendances d'un projet ne se limite pas toujours aux bibilothèques logicielles utilisées. La configuration des outils tiers fait partie intégrante de la mise en place d'un environnement de développement. Et lorsque les outils diffèrent sur les projets que l'on a en cours, ou que l'on doive en dépanner un avec une version différente de celle installée par défaut sur notre système, les ennuis commencent...

READMORE

Depuis mon arrivée chez [Level UP Solutions](https://levups.com), je suis un utilisateur de ~~OSX~~ macOS, et comme tout développeur sur cette plate-forme, j'utilise [Homebrew](https://brew.sh) pour installer les logiciels et utilitaires dont j'ai besoin pour mon travail et mes _side projects_. Or sur certains projets, nous sommes dépendants des versions fournies/installées sur les environnements de production que nous ne maitrisons pas toujours.

En utilisant Homebrew de façon classique, on spécifie quel logiciel on souhaite installer avec `brew install`, et régulièrement on fait des `brew upgrade` pour maintenir à jour les logiciels installés. Une options `brew pin` disponible pour bloquer les mises à jour d'un logiciel spécifique à la version installée initialement, ce qui peut s'avérer pratique si on souhaite garder une cohérence avec les outils installés en production qui ne suivent pas le même ryhtme de mise à jour (branches stables bien souvent).

Cependant, sur différents projets avec différentes versions de logiciels mis à jour (ou non !) à des rythmes différents, il devient un peu plus compliqué de répliqueri cela correctement sur notre environnement de développement.

### "Introducing ASDF"

Depuis un moment déjà, nous utilisons pour nos développements un système de gestion des version de ruby ([Rbenv](https://github.com/rbenv/rbenv)). Ce dernier permet de récupérer une version spécifique de ruby via les sources, de compiler la version localement et met en place toute une mécanique qui permet de spécifier globalement ou localement la version que l'on souhaite utliiser. Par exemple, par défaut je peux avoir la dernière version en date, mais dans un dossier contenant un projet d'un certain age, utiliser une version plus ancienne. Le simple fait d'être dans ce dossier suffit pour que le gestionnaire modifie les chemins d'accès aux exécutables, bibiliothèques et autres _gems_.

Eh bien ADSF fonctionne sur le même principe, avec des outils comme les bases de données, ou même les langages de programmation. En s'appuyant sur un système de _plug-ins_, ASDF est à même de proposer l'installation de différentes versions d'un même logiciel, de manière globale ou locale à un dossier.

Il suffit pour cela d'[installer le binaire `asdf`](https://github.com/asdf-vm/asdf#setup), ou de l'installer avec Homebrew pour les macOS-iens.

    brew install asdf

Notez bien les instructions affichées pour paramétrer correctment `asdf` à votre environnement[^1] puis il suffit d'ajouter les _plug-ins_ que l'on souhaite utiliser :

    adsf plugin-add redis

De lister les versions disponibles :

    asdf list-all redis | tail -5

Et de lancer une installation de la version demandée :

    asdf install redis 5.0.2

ASDF va alors récupérer les sources, et lancer une compilation du logiciel. Il va sans dire que cela nécessite les outils de compilation classique (autoconf, gcc, make, etc.) mais c'est un autre sujet.

L'outil redis est désormais disponible et pour l'utiliser globalement, il faut le préciser avec `asdf global redis 5.0.2`.

Maintenant imaginons que l'un de nos projets nécessite une version inférieure. Il suffit de placer un fichier `.tool-versions` contenant le nom du logiciel (dont le _plug-in_ a été ajouté précédemment) et la version que l'on souhaite :

    # ~/projects/mon_vieux_projet/.tools-versions
    redis 4.0.11

Placé dans ce dossier, il me suffit de taper la commande `asdf install` pour que ASDF se charge de la vérification et le cas échéant de l'installation des logiciels spécifiés selon la version désirée.

Vérifions que la configuration fonctionne comme attendue :

    $ cd ~/projects/mon_vieux_projet
    $ redis-cli --version
    redis-cli 4.0.11

    $ cd ~
    $ redis-cli --version
    redis-cli 5.0.2& 

Reste la configuration sur cette base du reste de votre environnement de développement. Il y a en la matière tellement de manière de faire que je réserverai la description du mien pour un autre billet.

Vous trouverez sur le _repo_ de ASDF [la liste des _plug-ins_ disponibles](https://github.com/asdf-vm/asdf-plugins). Et s'il en manque un à votre goût, pourquoi ne pas tenter le développement d'un à l'aide de l'[API](https://github.com/asdf-vm/asdf/blob/master/docs/creating-plugins.md) disponible ?

[^1]: pour mon cas, utilisant le _shell_ [Fish](https://fishshell.com/) par défaut, j'ai été contraint de placer certaines variables d'environnement dans un fichier commun à `fish` et à `bash` afin que les compilations fonctionnent correctement.
