---

title: Deux jours de formation à VueJS chez Sfeir School Lille
date: 2018-04-26 07:07 UTC
tags: work, dev, formation, vuejs 
cover: vue.png
---

Hé oui, il n'y a pas que Ruby dans la vie. Parfois, il faut savoir s'ouvrir sur d'autres technologies, d'autres façons de procéder, d'autres approches, pour parvenir à trouver de nouveaux ou de meilleurs moyens d'apporter la satisfaction dans l'usage d'un système que l'on développe pour nos clients.

READMORE

C'est dans cet esprit d'ouverture et de curiosité chère à notre [petite boutique](https://www.levups.com) que j'ai eu l'occasion de participer à la formation VueJS de chez [Sfeir School](https://school.sfeir.com/). Les formations qu'ils dispensent sont gratuites et ouvertes à tous les développeurs ayant les pré-requis annoncés.

![VueJS 200 Sfeir School](/blog/2018-04-26-formation-vuejs-sfeir-school-lille/vuejs-200.png)

Petite précision, ce billet n'est aucunement sponsorisé. Je le rédige par pur désir de publier un retour positif de cette expérience de manière un peu plus développée que via un misérable message sur les réseaux sociaux.

Pour cette formation, je partais quasi sans bagage, c'est à dire très peu d'expérience dans le domaine des _frameworks_ Javascript, ainsi que dans le domaine de Javascript tout court, mais c'était suffisant pour suivre cette formation. Il faut dire que nos besoins en terme d'interaction _front_ ont toujours été comblés par un usage très limité de Javascript. La combinaison de jQuery pour ses fonctions haut niveau de manipulation du DOM et de Coffeescript pour sa syntaxe assez proche de Ruby ont toujours été suffisant et s'intégraient bien dans notre workflow. Mais ce n'est pas une raison pour s'enfermer dans des acquis et aller voir ce qui se fait ailleurs peut parfois être intéressant pour de futurs évolutions/développements.

L'accueil a été très bon. Petit café/croissants de bienvenue par Agathe en charge de l'organisation, présentations respectives des participants et des formateurs Raphaël et Quentin. Deux formateurs, quel luxe !

L'entrée en matière est très rapide. Le _setup_ ayant été annoncé comme pré-requis à la formation et indiqué par mail quelques jours à l'avance, c'est très rapidement que nous mettons les mains dans le cambouis. Le rythme est très plaisant, quelques slides pour expliquer ce que nous allons réaliser sur les 2 jours de la formation, et c'est parti.

Chaque partie est découpée en séries de _slides_ pour expliquer un concept, suivie d'une application directe en exercice individuel, pour se conclure par une correction collective sur grand écran. L'enchainement est facilité par la mise à disposition d'un dépôt git avec autant de branches que de parties pratiques à réaliser. C'est assez malin car il n'y a pas de temps perdu à préparer ou synchroniser le code de chaque participants. Tout le monde part sur les mêmes bases pour chaque exercice et l'avancement dans le projet global de la formation est régulier et progressif.

Les concepts s'enchainent et c'est très rapidement que je découvre ce qui fait le succès de ce genre d'outils _frontend_. Tout d'abord, l'environnement de développement, à base de webpack et configuré à l'aide d'un outil en _CLI_ qui met en place le _linting_ et le _hotreload_. Cela rend le _feedback loop_[^1] très court et le développement très dynamique. Ensuite le coté temps réel de l'outil qui fait que modifier une variable déclenche la mise à jour des composants et des données liées directement dans le navigateur.

C'est directement satisfaisant lorsque par exemple on veut filtrer une liste de noms. À chaque lettre tapée dans la zone de saisie, la liste se met à jour simplement en indiquant que ce qui doit être affiché s'appuie sur une fonction qui renvoie la liste triée mise à jour à chaque événement identifié dans le code. Alors certes, on pourrait très bien le faire en Vanilla/jQuery, mais l'élégance de la déclaration et de l'organisation du code, ainsi que la performance affichée font qu'il est bien plus facile et serein d'envisager des interactions complexes améliorant grandement l'expérience utilisateurs que simplement en saupoudrant de jQuery ici ou là.

![VueJS filtering](https://cl.ly/0Z1w1R2f3y38/Screen%20Recording%202018-04-20%20at%2004.04%20PM.gif)

Bref, ce billet n'est pas là pour expliquer ou faire l'éloge de VueJS, mais j'avoue que j'ai été assez séduit par l'approche du _framework_.

Pour en revenir à la formation, au delà de ce qui nous est expliqué, l'ambiance est très bonne et très détendue. Les pauses et le repas du midi permettent de faire connaissance avec les autres participants, d'échanger sur nos expériences et nos usages, et de voir que nous avons tous plus ou moins globalement les mêmes problématiques quelque soient nos environnements.

Comme je l'ai précisé en début de billet, ces formations sont proposées gratuitement mais il n'y a pas de loup caché ni de prosélytisme « sfeir-ique ». Pour en avoir discuté avec Agathe, leur approche est plutôt basée sur le bouche-à-oreille et le réseau. Proposer des formations, des meetups, ou des _hackathons_/_code retrats_ et laisser les curieux faire le premier pas. Si jamais vous étiez intéressé par une mobilité professionnelle, vous pouvez toujours contacter [Sfeir](https://weare.sfeir.com/level-up/) ou vous inscrire pour [une formation](https://school.sfeir.com/) et discuter avec les formateurs et voir ou ça vous mène. Comme beaucoup de boîtes actuellement, ils recherchent dans tous les profils.

Ou sinon vous pouvez aussi vous inscrire et profiter de l'expérience pour la ramener chez vous et vous faire quelques contacts au passage, c'est tout aussi bien.

Merci en tout cas à l'équipe de Sfeir Lille pour leur accueil ainsi qu'à leurs formateurs très compétents et très ouverts. Si l'occasion m'en est à nouveau donnée, j'y retournerai bien volontiers.

[^1]: boucle de rétroaction dans le jargon spécialisé.
