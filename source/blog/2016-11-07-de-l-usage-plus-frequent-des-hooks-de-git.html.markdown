---
title: De l'usage plus fréquent des hooks de git
date: 2016-11-07 10:13 UTC
tags: dev, ruby, tests
cover: record.jpg
---
Je suis sur que, comme moi, vous n'utilisez pas ou peu le formidable pouvoir qui vous est accordé en tant que développeur par `git`.

READMORE


Ce matin, pour la nième fois, j'ai encore oublié un élément de déboggage dans ma suite de test. Et puis j'ai _commit_, et puis j'ai naïvement poussé sur le dépôt pensant que la suite de test (déclenchée par le _webhook_ de la CI) allait passer sans encombre sur cette petite modification mineure.

QUE NENNI!

C'était la fois de trop. Depuis longtemps, et à chaque fois que ça arrivait, je me disais : "Cette fois, j'ajoute un _hook_ git !", et je passais à autre chose de plus important. Mais ce matin, j'ai dégainé mon moteur de recherche et j'ai rapidement trouvé de quoi faire le job comme on dit.

```bash 
#!/usr/bin/env bash

set -e

CURRENT_DIR=$(pwd)
cd $(git rev-parse --show-toplevel)

COMMIT_FILES=$(git diff --cached --name-only)

for FILE in $COMMIT_FILES; do
  if [[ "$FILE" =~ ^.+\.rb$ ]]; then
    /usr/local/opt/rbenv/shims/ruby -c "$FILE" 1> /dev/null
    if [ $? -ne 0 ]; then
      echo -e "COMMIT REJECTED: files with syntax errors."
      exit 1
    fi
  fi
done || exit $?

TEST_FILES_PATTERN="_test.rb$"
FOCUS='^\s*focus'
echo $COMMIT_FILES | \
    grep -E $TEST_FILES_PATTERN | \
    xargs grep --with-filename --color=never -n $FOCUS && \
    echo "COMMIT REJECTED: found 'focus' references in test files" && exit 1

cd $CURRENT_DIR
```

Voila, avec ceci installé dans le répertoire `.git` du projet, plus moyen de commit si la syntaxe des fichiers est erronée ou si j'ai laissé un `focus` dans les tests. Restera plus qu'à le compléter au fil de l'eau et de mes déconvenues avec la _CI_. Peut-être l' "industrialiser" avec un système de fichier de configuration de pattern exclus. Oh mais tiens, ça [existe](https://github.com/marick/pre-commit-hooks) [déjà](https://github.com/jandre/safe-commit-hook) !

**MaJ du 07 novembre 2016 – 15:22**: [@GromNaN](https://twitter.com/GromNaN) me signale à juste titre que l'exemple donné ici en `bash` [n'est pas une obligation](https://twitter.com/GromNaN/status/795617704128479232). Les _hooks_ peuvent être écrit dans n'importe quel langage supporté par le système (Perl, Python, PHP, ruby, Go, Rust, etc.)

**Erratum du 08 novembre 2016 – 08:22**: manifestement le bout de script permettant de détecter la présence de `focus` ne fonctionne pas, si vous avez une correction, [je suis preneur !](https://gitlab.com/bobmaerten/bobmaerten.eu) Mais l'idée du billet est toujours là. Intéressez vous au _hooks_ de `git`, et surtout ne prenez jamais pour argent comptant ce que vous glanez sur le net !

