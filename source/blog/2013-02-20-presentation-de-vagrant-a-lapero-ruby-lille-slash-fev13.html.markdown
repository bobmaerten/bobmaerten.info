---
title: Présentation de Vagrant
date:  2013-02-20
cover: trainstation.jpg
tags:  talk, slides, vagrant, devops
---

J'ai eu l'occassion de parler un peu de mon usage de [Vagrant](http://vagrantup.com) lors de l'apéro-ruby Lille de février 2013, aussi voici [les quelques slides](https://speakerdeck.com/bobmaerten/vagrant-101) que j'ai présenté.

[![Vagrant 101](https://speakerd.s3.amazonaws.com/presentations/ae9492e05d5d013038ac12313916f0ac/slide_0.jpg?1361345767)](https://speakerdeck.com/bobmaerten/vagrant-101)

Speakerdeck ne permettant apparemment pas de cliquer sur les liens fournis dans la présentation, voici [la version PDF de ces slides](https://speakerd.s3.amazonaws.com/presentations/ae9492e05d5d013038ac12313916f0ac/vagrant-101.pdf).

Évidemment cela n'inclut pas les questions/discussions qui s'en sont suivies, donc si vous avez des questions, n'hésitez pas à les poser dans les commentaires ou directement par mail.
