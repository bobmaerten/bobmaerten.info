---
title: Pour une meilleure position de travail
date: 2018-03-31 15:32 UTC
tags: work, life
cover:
---

Il y a 4 ans, je décrivais un _[proof of concept](/i-m-still-standing-desk-and-cycling)_ qui me permettait d'alterner les positions assises et debout pour travailler. C'était à l'époque assez rudimentaire mais j'avais largement apprécié l'expérience.

READMORE

Aujourd'hui, à l'occasion de la réfection de mon environnement de travail à la maison, j'ai investi dans un nouveau bureau qui permet de retrouver ces sensations tout en prenant beaucoup moins de place et surtout en gardant mes affaires en place. Je veux bien évidemment parler d'un bureau à hauteur réglable.
![](2018-03-31-position-travail-assis-debout-bureau-electrique/setup_debout.jpg)

Les pieds sont de la marque [Flexispot](https://www.amazon.fr/stores/node/12454448031) qui ont l'avantage d'être distribués sur Amazon. Pour le plateau, après un long moment à hésiter à en prendre un en bois brut, je me suis rabattu sur un [plateau alvéolaire](https://www.ikea.com/fr/fr/catalog/products/20353741/) de chez Ikea.

Les pieds sont fournis avec une commande qui permet de mémoriser 3 hauteurs de travail différentes, ainsi qu'une sorte d'alarme qui rappelle de changer de position.
![](2018-03-31-position-travail-assis-debout-bureau-electrique/commande.jpg)

J'avoue que je suis particulièrement satisfait du _cable management_, réalisé à l'aide d'adhésif double face et des attaches autocollantes également fournies avec les pieds.
![](2018-03-31-position-travail-assis-debout-bureau-electrique/cable_management.jpg)

Bref, après une semaine d'usage, je dois dire que je suis très content de mon investissement et que cette possibilité d'alterner les positions assise et debout est très agréable. Mon dos ne s'en portera que mieux !