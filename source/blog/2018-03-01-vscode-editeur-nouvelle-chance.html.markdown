---
title: Nouvel essai d'utilisation de Visual Studio Code au quotidien
date: 2018-03-01 20:02 UTC
tags: dev, work, ruby, rails
cover: code.png
---

Depuis quelques temps, je trouve que l'usage de Atom dans le cadre de mon travail quotidien plutôt poussif.
Aussi je me suis dis que j'allais redonner une nouvelle chance à un autre éditeur de temps à autres. J'ai choisi de
tester à nouveau Visual Studio Code de Microsoft, que j'avais tenté d'utiliser il y a quelques temps
mais pour lequel je n'avais pas trouvé toutes les extensions qui me sont utiles pour travailler efficacement.

READMORE

## TL;DR
Ça fait quelques jours que je m'y tiens et c'est de la balle, je comprends que pas mal de monde s'en serve. 
Je retrouve des reflexes et des sensations de performance que j'avais mises de coté avec Atom.
Vous trouverez la liste des extensions que j'utilise à la fin de ce billet.

## Cheminement

Mon premier essai de VSCode ne m'avait en effet pas convaincu. Mon passage de Vim à Atom depuis quelques temps me convenait
assez, l'éditeur n'était pas des plus réactifs parfois mais tournait somme toute de manière convenable et surtout j'avais
trouvé toute une panoplie d'extensions qui augmentait soit ma productivité, soit mon confort, souvent les deux à la fois.

Rien n'indiquait qu'un changement voire même qu'une étude d'un autre éditeur était à envisager. Cependant j'ai été 
confronté récemment à des soucis de performance et de surchauffes soudaines de mon ordinateur portable, qui au final
ont incriminé plus ou moins directement Atom.

Le dévelopment web actuel implique l'usage de pas mal d'outils de nos jours. Même dans une configuration simple comme celui
de ce blog, j'ai un certain nombre d'outils qui tournent en tâche de fond, afin de _build_-er en continu les éléments qui
constituent le site. Dans le cadre de mon travail en Ruby/Rails, j'utilise en particulier `spring` qui permet de garder
l'application Rails en mémoire pour améliorer la réactivité des _builds_ et des tests. J'utilise également `ctags` un analyseur de
symboles de code qui permet d'indexer l'emplacement des variables, méthodes et autres références pour les retrouver plus
facilement. Cet index est automatiquement mis à jour lorsque qu'un fichier est sauvegardé dans l'éditeur.

Il s'avère que pour une raison que j'ignore toujours, le déclenchement de cette indexation faisait partir `spring` en boucle, jusqu'à
à prendre 100% du processeur, et chose que je déteste, à mettre en route la ventilation bruyante de l'ordinateur portable.

Je ne sais pas si c'est un souci de Atom qui ne lance pas correctement `ctags`, ou si c'est le système de surveillance des fichiers (`listen`)
utilisé par `spring` qui ne gère pas correctement les appels aux fichiers, mais en être arrivé au point d'avoir un alias pour tuer le processus `spring`
est je pense un bon indicateur qu'il faille faire quelque chose.

J'ai bien essayé de regarder/_patch_-er `spring` ou `listen` pour exclure les fichiers générés par `ctags`, mais bon,
je ne suis même pas sur de pouvoir clairement isoler le problème, ni même avoir une idée assez claire pour ouvrir un
ticket sur un des projets en question, ni même savoir sur lequel le poser.

Bref, comme il est toujours bon d'aller voir de temps en temps ce qui se passe « chez la concurrence »,
je me suis dit pourquoi pas essayer autre chose après tout. Et comme je vois de plus en plus de gens se servir de VSCode,
je voulais savoir qu'est-ce qui pouvait faire en sorte que cet outil attire autant de devs.

## Deuxième approche de VSCode

Comme je le disais plus haut, j'avais déjà essayé VSCode. Enfin, essayé est un grand mot, autant dire que je l'avais installé,
lancé, essayé quelques fonctionnalités, pesté contre le fait que le _mapping_ des commandes était différent de Atom/Sublime,
et _ragequit_-é l'application. Ok, j'ai également pris un peu de temps pour voir s'il existait des extensions utiles à mon workflow
Ruby/Rails habituel, mais je trouvais qu'il y avait une forte orientation de cet éditeur au dévelopment ECMAScript et la liste des
extensions disponibles en était effectivement le reflet.

Mais du temps à passé et l'éditeur a gagné en maturité, en utilisateurs et les différents communatutés de dévelopeurs ont mis
leur touche via des extensions moins mono-ciblées JS.

La documentation est également très fournie. Il y a pas mal d'[articles](https://code.visualstudio.com/docs) et même des vidéos sur
[la mise en route](https://code.visualstudio.com/docs/getstarted/introvideos),
[la configuration](https://code.visualstudio.com/docs/getstarted/settings), ainsi que des guides et des extensions pour 
faciliter la transition depuis un autre éditeur en reprenant les mappings des principaux éditeurs du marché.

J'ai fait le choix de ne pas utiliser les _mappings_ de Atom pour essayer d'apprendre ceux par défaut de VSCode (j'en ai
modifié/adapté quelques uns tout de même). On peut en effet personaliser les raccourcis associé à des fonctions et la recherche
des fonctions de l'éditeur est très pratique pour cela.

L'éditeur reprend le système de configuration de SublimeText, avec un double panneau listant toutes les configurations par défaut, et 
un panneau avec les personalisations. On peut même sélectionner une valeur et la recopier pour la modifier assez facilement. Il y a même
une différentiation des élements de configuration globale et de ceux limités au projet en cours.

Bref, pas mal de bon dans cette redécouverte de VSCode. Cependant, il reste toujours des zones à améliorer. En particulier
la recherche textuelle. Comparée à celle de Atom avec le choix d'affichage des lignes précédentes et suivantes, et même le simple
fait que celle ci s'effectue dans la zone principale et non dans la zone de l'explorateur de fichiers. Trop étroit.

Idem pour l'intégration `git`, bien que plutôt bien fichue, celle-ci opère dans la même zone. Mais pas de commit partiel de fichier,
ni de _amend_ possible. Pratique pour un commit rapide sans quitter l'éditeur, mais pour un peu plus de souplesses, Atom brille
bien mieux dans ce domaine, même s'il y a toujours moyen de passer par la ligne commande (VSCode propose un système de terminal intégré),
ou par un outil externe comme `gitup` ou GitHub Desktop. On m'a conseillé de jeter un oeil à l'extension GitLens, mais je n'ai pas
encore eu le temps de m'y investir réellement. Cela semble assez complet et complexe.

Au final, tous ces points négatifs sont largement compensés par la meilleure réactivité de l'outil par rapport à Atom,
ainsi que par l'ajout de quelques extensions assez bien adaptées à mon _workflow_.

## Les extensions « indispensables » à mon usage

| Nom | Version | Commentaire |
| --- | --- | --- |
| Align | 0.2.0                   | le seul plugin d'alignement de code que j'ai trouvé qui prenne en compte les `hash` ruby avec des _symbols_ |
| code-settings-sync | 2.8.7      | Synchro de la configuration/paramètres/plugins via un Gist secret |
| EditorConfig | 0.12.1           | Respect des normes de formattage des fichiers |
| endwise | 1.2.2                 | Ajoute automatique des `end` lorsqu'on tape un do/if/def |
| polacode | 0.2.2                | screenshot d'un morceau de code avec le theme courant |
| prettier-vscode | 1.1.3         | Le _linter_ ultime de nos jours |
| rails-flip-flop | 0.0.2         | bascule rapide entre le fichier courant et le fichier test associé (j'ai ajouté un shortcut à la main) |
| rails-latest-migration | 0.0.1  | accès rapide au dernier fichier de migration rails |
| rails-migration-list | 0.1.2    | liste et filtrage dans les migration rails |
| rails-partial | 0.0.1           | extrait un bout de code dans un partial |
| rails-snippets | 1.0.6          | quelques raccourcis pour rails |
| Ruby | 0.16.0                   | _mandatory_ extension, intégration avec le débuggeur |
| ruby-rubocop | 0.7.0            | Rubocop à la demande ou en continu sur le fichier en cours |
| ruby-snippet | 0.1.0            | quelques raccourci pour ruby genre map -> map { |e| } |
| sass-indented | 1.4.8           | _lint_/colorisation des fichiers sass |
| simple-ruby-erb | 0.2.1         | quelques raccourcis pour erb genre auto fermeture des tags <% |
| slim | 0.1.2                    | colorisation/_lint_ des fichiers slims |
| theme-railscasts | 3.0.0        | Le moins pire des themes railscasts que j'ai essayé, proche de ce que tu as fait pour Atom pas pas tout a fait |
| vscode-dash | 1.8.0             | raccourci vers l'outil de documentation offline Dash |
| vscode-fileutils | 2.8.1        | essentiellement pour le duplicate file/folder qui permet de modifier le chemin complet |
| vscode-great-icons | 2.1.26     | Oh les belles icones |
| vscode-quick-select | 0.2.5     | Étend la sélection au caractère englobant indiqué (parenthèse, quote, double quote, etc.) |
| vscode-toggle-quotes | 0.2.0    | Inverse les simples quotes et les doubles quotes englobante d'une chaine ou se situe le curseur |
| wrapSelection | 0.6.8           | permet d'englober la sélection du caractère indiqué (parenthèse, quote, double quote, etc.) |

