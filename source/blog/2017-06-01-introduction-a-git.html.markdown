---
title: Introduction à git
date: 2017-06-01 19:34 UTC
tags: git, slides
cover: sliders.jpg
---
Le dernier enregistrement[^1] du podcast [Le Ruby Nouveau](https://lerubynouveau.fr) avait pour thème la gestion de versions du code source. Ça m'a rappelé que j'avais créé un certain nombre de slides, un peu avant mon départ de l'Université pour le privé, afin de propager la bonne parole de la gestion de version avec git à mes anciens collègues sous la forme d'une longue session de présentation / questions / réponses.

READMORE

En remettant le nez dans ces 235 slides, il s'avère que ceux ci sont toujours valables et potentiellement instructifs, même pour ceux qui manipulent git au quotidien. Ils détaillent les arcanes de l'outil git (ce qu'on appelle la partie porcelaine) pour essayer d'expliquer le plus simplement possible le pourquoi du comment du parce que de l'usage des commandes courantes. 

C'est pourquoi je vous les propose ici même, ou sur SpeakerDeck si vous préférez, à consommer à loisir. Je suis certain que cela va vous pousser à chercher plus loin sur certaines parties.

-----

[Parcourir les slides](https://blog.bobmaerten.info/slides/presentation-git/index.html)

-----

<script async class="speakerdeck-embed" data-id="c2778dba8a2843ce8d8a34da74c2a77f" data-ratio="1.41436464088398" src="//speakerdeck.com/assets/embed.js"></script>

[^1]: cet épisode devrait être disponible le 12 juin 2017.
