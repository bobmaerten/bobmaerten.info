---

title: Quelques semaines après l'installation d'un Pi-hole en remplacement des services DNS/DHCP à la maison
date: 2019-05-08 10:00 UTC
tags: devops sysadm web materiel
cover: pihole.jpg
---

TL;DR: ça fonctionne très bien.

Intrigué depuis quelques temps par ce projet qui permet de « centraliser » le filtrage que l'on installe habituellement via une extension de navigateur, j'ai eu envie d'expérimenter le système à l'occasion d'une promotion sur un [Raspberry Pi 3 B+](https://www.raspberrypi-france.fr/raspberry-pi-3-b-plus/).

READMORE

#### Installation

Je passe sur l'installation du matériel qui est somme toute assez simple (insérer la carte mère dans un boitier adapté et brancher), et très rapidement sur l'installation du système de base qui consiste à aller sur la [page de téléchargement officielle de Raspbian](https://www.raspberrypi.org/downloads/raspbian/) pour chopper l'image (_lite_) à installer sur la carte SD en suivant les [instructions de la documentation](https://www.raspberrypi.org/documentation/installation/installing-images/mac.md) (pour macOS).

Insérez la carte dans le Pi et connectez-le à l'alimentation avec un clavier et un écran pour l'installation du système. Une fois la procédure terminée, identifiez-vous avec les _credentials_ par défaut, puis changez tout de suite le mot de passe de l'utilisateur `pi`.

Petit conseil : pensez à déposer un fichier (vide) nommé `SSH` à la racine de la partition de _boot_. Cela permet d'activer le service SSH qui n'est pas configuré par défaut, et ça vous permettra de pouvoir accéder au système en console, une fois votre Pi installé dans votre « local technique ».

Ensuite, il n'y a plus qu'à suivre les instructions de la [documentation de pi-hole](https://docs.pi-hole.net/main/basic-install/) et c'est presque terminé. Franchement je m'attendais à bien plus de bidouille pour arriver à avoir quelque chose d'utilisable, mais non, ça « juste marche » comme on dit.


#### Test

![« Baie » réseau de mon domicile](2019-05-08-avis-post-installation-pi-hole-dns-dhcp/reseau.jpg)

Une fois le _software_ installé, j'ai pu donc « planquer » le Pi dans un coin de mon installation réseau et y accéder depuis l'interface qui permet de faire tout ce dont on a besoin pour configurer le filtrage. Par défaut, le pi-hole est un service sur votre réseau domestique qui ne fait rien tant qu'on ne l'a pas configuré pour.

En effet le principe de pi-hole est d'intercepter les requêtes DNS et de vérifier dans ses tables de correspondances si le domaine est marqué sûr ou non sûr, auquel cas le service va renvoyer l'adresse IP associée au nom demandé ou pas.

Pour commencer, j'ai donc seulement modifié les paramètres DNS en manuel sur mon ordinateur de travail pendant une semaine, le temps de vérifier que tout fonctionnait correctement avant de « mettre en production » pour toute la maison. Ce qui fût assez concluant, les sites « courants » (google ads/analytics, doubleclik et consorts, etc.) sont bloqués au niveau réseau, plus besoin d'adblock sur le navigateur. Je n'ai quasiment pas eu de soucis ressenti, mis à part Spotify qui se bloque complètement lorsqu'il tente d'accéder à son service de publicité en vidéo (pour les pubs audio, ça fonctionne correctement). J'ai donc été assez confiant rapidement pour le généraliser à tous les postes informatiques de la maison.

#### Configuration

L'interface d'administration de pi-hole est très facile d'accès. Bon bien sur il faut avoir des bases de réseau, mais concrètement la seule chose que j'ai eu à configurer est la plage d'adresses pour le DCHP, l'adresse du routeur et le nom de domaine utilisé pour le réseau local. Ensuite j'ai désactivé la fonction DHCP de mon routeur afin que le Pi prenne le relai.

![Interface admin DHCP de pi-hole](2019-05-08-avis-post-installation-pi-hole-dns-dhcp/dhcp_admin.png)

Et voilà… tous les appareils qui se connectent désormais au réseau (filaire ou WiFi) obtiennent leur configuration réseau du pi-hole et sont donc protégés des méchants _trackers_ et autres domaines indésirables.

Il est bien évidemment possible de modifier ce comportement de base, en ajustant les listes de filtrage utilisées, en marquant manuellement des domaines sûrs ou non-sûrs, il est même possible de désactivement temporairement le filtrage. Cela m'a été utile une fois alors qu'une procédure de paiement en ligne sur un site bloquait pour une raison obscure, j'ai désactivé le filtrage 30 secondes le temps de procéder au paiement et c'est passé, tranquille.

![Désactivation temporaire du filtrage](2019-05-08-avis-post-installation-pi-hole-dns-dhcp/temp_disable.png)

#### Avis

Cela fait donc quelques semaines que ce système « ronronne » et je n'ai vraiment eu que quelques soucis avec certaines applications mobiles un peu intrusives et cerrtains jeux sur tablette qui ne fonctionnaient plus correctement, mais à part cela l'usage du web est relativement semblable à celui lorsque j'avais un adblock sur mon navigateur. Cette logique est désormais déportée sur le pi-hole qui « fait l'boulot » et qui libère nos appareils de ce besoin absolu de filtrage de nos jours. Le web est plus propre, et plus léger surtout.

Et ça filtre ! L'interface d'admin est accessible en mode non identifié et affiche les statistiques de blocage.

![Statistiques de filtrage](2019-05-08-avis-post-installation-pi-hole-dns-dhcp/block_stats.jpg)

L'interface indique même quelques informations sur le système (température, charge et consommation mémoire). Et les chiffres semblent indiquer qu'il est tout à fait envisageable de faire tourner d'autres choses sur le Pi.

Là ou ça se complique, c'est lorsque j'utilise le web hors de mon domicile. Retrouver tous ces « intrus » en mobilité… c'est vraiment dingue de se rendre compte à quel niveau les gens qui ne sont pas sensibilisés doivent subir cela au quotidien !

Du coup, la prochaine étape sera probablement de configurer un accès VPN pour pouvoir me retrouver comma à la maison lorsque je suis en mobilité. Mais c'est une autre histoire. 😉
