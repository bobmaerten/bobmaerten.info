---
title: Encore un nouveau look !
date: 2016-04-24 08:03 UTC
tags: blog
cover: cabs.jpg
---

Bon, eh bien comme je le disais dans [un précédent billet](/un-nouveau-look-pour-le-blog), j'hésitais entre 2 thèmes pour le rafraichissement de facade de ce blog, et j'ai fini par choisir le thème [Casper](https://github.com/danielbayerlein/middleman-casper) en passant le pas de l'usage de la version 4 de [Middleman](https://middlemanapp.com/).

READMORE

En effet, j'ai eu beaucoup de mal à opérer la transition avec le thème précédent, notamment au niveau des images (un vrai calvaire avec les liens relatifs), alors j'ai _rage-erased_ la configuration et suis reparti d'un bon vieux `middleman init` pour faire propre.

J'en ai également profité pour corriger de-ci de-là, quelques erreurs sur certains anciens billets, qui en les relisant ne sont plus vraiment à jour, mais c'est l'intérêt de ces petites choses, pouvoir regarder en arrière et voir ce qui a changé.
