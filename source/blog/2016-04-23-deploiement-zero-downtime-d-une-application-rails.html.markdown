---
title: Déploiement zero-downtime d'une application Rails
date: 2016-04-23 11:59:46 UTC
tags: talk, ruby, dev, linux
cover: mba2.jpg
---
Lors du dernier apéro [Ruby Nord](http://ruby-nord.org), j'ai eu l'opportunité d'exposer les concepts du déploiement d'application Rails sans interruption de service.

READMORE

[![Talk Avril 2016](2016-04-23-deploiement-zero-downtime-d-une-application-rails/talk_avril_2016.jpg)](https://speakerdeck.com/bobmaerten/deploiement-dune-application-rails-sans-interruption-de-service)

[Les _slides_ de ce _talk_](https://speakerdeck.com/bobmaerten/deploiement-dune-application-rails-sans-interruption-de-service) se trouvent sur SpeakerDeck, et les [sources](https://gitlab.com/bobmaerten/presentation-zdd) sont disponibles chez GitLab.
