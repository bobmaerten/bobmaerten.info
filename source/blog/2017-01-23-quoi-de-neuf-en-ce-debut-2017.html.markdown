---
title: Quoi de neuf en ce début 2017&nbsp;?
date: 2017-01-23 08:14 UTC
tags: life, blog
cover: mba1.jpg
---
Un peu d'actualité pour ce premier billet de l'année. Il n'est pas encore trop tard pour faire un bilan ou prendre de bonnes résolutions, non&nbsp;?

READMORE

Naaaaaaaaaann, je rigole !

## État des lieux

Eh oui, toujours présent même si de moins en moins loquace sur le contenu. Non pas que j'eusse peu de choses à dire, mais [d'autres](http://www.sois-net.fr/) [le font](http://lkdjiin.github.io/) [tellement mieux](https://www.synbioz.com/blog) que je ne pourrais jamais le faire... Alors je me contente essentiellement de publier mes découvertes personnelles et non pas recopier plus ou moins maladroitement mes sources.

Tiens peut-être devrais je tout simplement poster des compilations des nombreux liens qui m'ont bien dépannés au quotidien ?

## Projets

À titre personnel, j'ai toujours de vagues idées d'outils à implémenter, comme un système de commentaires pour blog statique, ou un outil qui collerait à ma façon de faire de la veille techno, mais à chaque fois que j'essaie de me lancer, je tombe sur un outil/projet déjà prêt ou bien avancé dont les fonctionnalités ne sont pas complètement éloignées de ce que je souhaite, mais pas complètement en phase à ce que j'attends. Alors je mets ça de côté et je me trouve une excuse bidon pour ne pas m'y mettre vraiment.

Cela dit, je ne suis pas sans activité non plus. Outre le démarrage des [meetups sur Valenciennes](https://www.meetup.com/fr-FR/dev-and-talk-valenciennes/) qui me permettent de rencontrer [des](https://twitter.com/seeyoucloud) [gens](https://twitter.com/jibundeyare) [sympas](https://twitter.com/DlpMichel), avec quelques personnes du [Slack de ParisRb](http://parisrb-slack-invite.herokuapp.com/) ainsi que [d'](https://twitter.com/_flexbox)[autres](https://twitter.com/r3trofitted) de [RubyNord](http://ruby-nord.org/), nous avons lancé un podcast audio&nbsp;: [Le&nbsp;Ruby&nbsp;Nouveau](http://lerubynouveau.fr). 

Il s'agit d'une émission débat/opinions regroupant quelques rubyistes autour d'un thème pendant 30-45mn. Nous avons déjà enregistré quelques épisodes à l'avance avec [des](https://twitter.com/_toch) [gens](https://twitter.com/gayahel) [fort](https://twitter.com/soveran) [agréables](https://twitter.com/olivierlacan) pour pouvoir nous permettre une diffusion régulière. N'hésitez pas à suivre l'actu sur le compte twitter associé : [@lerubynouveau](https://twitter.com/lerubynouveau), et à nous solliciter si vous vous sentez l'envie de nous rejoindre pour un épisode ou plus si l'endroit vous plait !

## Professionnel

Toujours bien et bien occupé chez [Level Up Solutions](https://levups.com) où chaque semaine j'ai l'opportunité de bosser sur des choses très sympa, quoique parfois prises de tête, mais toujours très gratifiantes à la fin. Je me rends bien compte que bosser pour des gens à la fois intéressants et compréhensif de nos méthodes de travail (agilité, qualité du code, fiabilité grâce aux nombreux tests, etc.) est une chance, mais aussi un frein pour trouver de nouveaux clients.

Toujours en _full remote_ mais toujours aussi peu mobile (l'hiver et le froid actuel n'aidant vraiment pas). Cela dit, le projet d'[espace de coworking](http://www.valenciennes-metropole.fr/innovation-amenagement/rives-creatives-de-lescaut/nouvelle-forge/) sur Valenciennes s'apprête à sortir enfin, et je compte bien aller le fréquenter régulièrement cette année. Ainsi que d'aller squatter chez tout ceux qui voudront bien m'accueillir à l'occasion d'un passage sur Lille. Oui, oui, je n'ai pas oublié les propositions [de](https://derniercri.io/) [certains](https://www.synbioz.com/)…
