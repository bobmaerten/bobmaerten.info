---
title: 'Rails5 : tester un contrôleur acceptant un payload JSON'
date: 2016-08-30 19:30 UTC
tags: dev, ruby, rails5, tests
cover: coffeebeans.jpg
---
Depuis la version 5 de Ruby on Rails, la méthode habituelle pour tester des contrôleurs et dépréciée en faveur de l'utilisation de _hash parameters_.

READMORE

À savoir que dans une classe héritée de `ActionController::Testcase`, les tests d'actions :

```ruby
get :index

@request.env['HTTP_REFERER'] = users_url
get :show, id: 1

post :create, album: { title: 'Plop' }
```

sont désormais remplacés par la syntaxe suivante :

```ruby
get :index, params: {}

get :show, params: { id: 1 }, env: { 'HTTP_REFERER' => users_url }

post :create, { album: { title: 'Plop' } }
```

Le différents paramètres utilisables étant `params`, `headers`, `env`, `xhr` et `as` ([source](https://github.com/rails/rails/blob/3e45dd988cad539fe2cbc79b505fde99a44472e8/actionpack/lib/action_dispatch/testing/integration.rb#L326)).

Cependant, alors qu'il était possible d'associer un _body_ à une requête de type `POST` avec `post :create, json_payload`, avec la nouvelle syntaxe ce n'est plus possible et pire, ce n'est même pas documenté.

En tâtonnant et en fouillant les internets, je suis tout de même arrivé à une solution. L'idée est de suivre les recommendations de `ActionDispatch::IntegrationTest` et remplacer le test de contrôleur par un test d'intégration en utilisant les _route helpers_ et en “trichant” sur le _hash_ `params` :

```ruby
post user_url, params: json_payload

patch user_url(@user), params: json_payload
```

Je ne sais pas si c'est la bonne méthode, ni si cette méthode resulte plus du hasard qu'autre chose, et encore moins si ce truc perdurera sur les prochaines _releases_ de `ActionDispatch`. Mais j'aimerais franchement qu'il y ait une solution plus élégante !

En attendant, J'espère que si comme moi, vous coincez sur ce genre de problème, un moteur de recherche viendra indexer cette page pour vous !
