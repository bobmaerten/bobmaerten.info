---

title: Des envies de renouveau photographique
date: 2018-05-22 07:35 UTC
tags: life, photo
cover: camera.jpg

---

Avec les beaux jours qui reviennent, et les vacances qui approchent, ça fait quelques semaines que reprendre la photo me titille. Et quand je ne me sens pas d'aller _shooter_, je regarde les nouveautés coté appareils photo numériques.

READMORE

<a title="By Nebrot [GFDL (http://www.gnu.org/copyleft/fdl.html) or CC BY-SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0)], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Canon_EOS_30D.jpg"><img width="512" alt="Canon EOS 30D" src="https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Canon_EOS_30D.jpg/512px-Canon_EOS_30D.jpg"></a>

En effet, mon bon vieux Canon 30D vient de fêter ses 12 ans de services, et même s'il fait toujours de beaux clichés pour lesquels il arrive même qu'on me félicite (mais merci plutôt le 17-40mm f/4), sa définition de 8 megapixels et sa sensibilité limitée à 1600 ISO commencent à se faire sentir. Et puis bon, 12 ans quand même, on peut bien s'accorder un petit plaisir par décennie, non ?

Du coup je suis allé à la Fnac ce weekend pour manipuler les boitiers qui me font de l'oeil en ce moment : Fujifilm X-T20, Canon 80D/6DmkII, Panasonix GX80 voire GH-4 et Sony A7III (pas le même budget certes, mais bon, tout le monde en parle...)

<a title="By Hurk87 [CC BY 4.0 (https://creativecommons.org/licenses/by/4.0)], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Fujifilm_X-T20.jpg"><img width="512" alt="Fujifilm X-T20" src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/Fujifilm_X-T20.jpg/512px-Fujifilm_X-T20.jpg"></a>

Grosse déception arrivé au rayon photo. Les hybrides Pana et Fuji sont tous en panne de batterie, tu peux juste donc prendre la chose en main et c'est tout vu que tout est électronique. Mais fichtre qu'il est petit ce X-T20 ! La prise en main me parait bancale par contre, il n'y a pas vraiment de grip. Du coup ça tient a peine dans la main et à part avoir un objectif assez conséquent, t'as vraiment l'impression de « jouer » avec un compact.

<a title="By Takeaway [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:2015_0826_Panasonic_GX8_03.jpg"><img width="512" alt="2015 0826 Panasonic GX8 03" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/69/2015_0826_Panasonic_GX8_03.jpg/512px-2015_0826_Panasonic_GX8_03.jpg"></a>

Idem pour le GX80, ça parait minuscule mais la prise en main me parait un poil meilleure. Le GH-4 a un vrai grip et est plus massif, ça se rapproche clairement d'un bridge/reflex.

<a title="By RBGArbga [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:GH4_crop_300px.jpg"><img width="256" alt="GH4 crop 300px" src="https://upload.wikimedia.org/wikipedia/commons/c/c1/GH4_crop_300px.jpg"></a>

Autre grosse déception, pas de Sony A7III, ni de II d'ailleurs, mais seulement un alpha7 de 2013. L'écran du viseur est horrible, peu lumineux, et ça balaye grave. Par contre ça se manipule pas trop mal, y'a une vraie poignée certes moins protubérante par rapport au GH-4 et encore moins rapport à mon reflex mais ça se prends bien mieux en main que le Fuji. L'interface semble intuitive et j'ai pu tester les rafales : « _Jeez, Louise !_ », ça capte ! J'imagine à peine les évolutions que ça a du donner sur le A7III. Apparemment, les boitiers sony intègrent une DB des objectifs et appliquent automtiquement aux JPG les correctifs pour pallier les défauts (vignetage, distortions, etc.). Ça fleure bon tous ces trucs chez Sony, à surveiller... Dommage que ce soit vraiment hors de prix.

<a title="By Hans Braxmeier [CC0], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Sony_Alpha_7.jpg"><img width="512" alt="Sony Alpha 7" src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/dd/Sony_Alpha_7.jpg/512px-Sony_Alpha_7.jpg"></a>

Enfin petit tour coté reflex. _Ouch !_ L'encombrement choque après avoir manipulé ces petites bestioles, mais je retrouve mes sensations de prise en main, tout tombe sous les doigts direct. Je teste le 77D, et la le choc : le viseur est minusucle, c'est inutilisable. Soit c'est vraiment pensé pour le liveview, ou alors le viseur était cassé ?

<a title="By GodeNehler [CC BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0)], from Wikimedia Commons" href="https://commons.wikimedia.org/wiki/File:Canon_80D-2.jpg"><img width="512" alt="Canon 80D-2" src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/3a/Canon_80D-2.jpg/512px-Canon_80D-2.jpg"></a>

Je tente le 80D et la le choc, mais à l'inverse. Ce viseur !!!! <3 <3 <3 Rien a voir avec le viewfinder électronique qui flick et pique les yeux du alpha7, du vrai bonheur. Je teste les fonctions que je connais bien, je découvre les nouveautés par rapport à mon 30D, c'est du terrain connu et de l'évolution lente mais itérative à la Canon. La rafale déchire, le mode silencieux, c'est pas mal non plus. La mise au point est très rapide avec pleins de collimateurs dans le viseur, dommage, pas de grille de composition dans le viseur, ni de niveau pour moi qui suis oligé de redresser toutes mes photos. L'écran sur pivot est un vrai plus. Oh et à coté un 6DmkII... mais plus de batterie. Je le prends en main tout de même. Vache, même calibre que le 80D, mais avec un capteur fullframe. Le viseur me semble un peu moins bon cela dit, bizarre.

Bon voila, du coup je ne suis guère plus avancé sur un choix pour renouveler mon matos photo, mais j'ai pu au moins manipuler les appareils que je vois en test dans les articles et videos du net depuis des semaines et me faire un début d'idée sur ces nouveaux hybrides qui évoluent bien plus vite et semble bien plus dans l'ère du temps que ces reflex lourds, encombrants, à la connectique dépassée (quelle honte ce port usb2 sur le canon 6DmkII).

Et vous, vous avez un avis sur la question, est-ce le bon moment de changer ?  Faut-il encore attendre encore un peu que la concurrence réagisse ? Ou alors simplement me rabattre sur appareil compact pour partir en vacances léger ?
