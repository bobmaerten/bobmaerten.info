---
title: Le Ruby Nouveau est arrivé !
date: 2017-01-31 07:35 UTC
tags: life, podcast, ruby
cover: lerubynouveau.png
---
Le Ruby Nouveau est un podcast audio sur la tech et le web mais essentiellement Ruby que nous avons commencé à produire pendant les vacances de Noël avec quelques volontaires. Il y a encore un peu de travail sur le site mais nous avons quelques épisodes en stock pour une diffusion hebdomadaire. Voilà au moins le [flux RSS](https://lerubynouveau.fr/feed.xml) pour votre lecteur de podcast préféré en attendant de le publier sur les différentes plates-formes de diffusion.

N'hésitez pas à nous remonter vos avis et nous solliciter si vous souhaitez intervenir dans nos prochains épisodes ou proposer un sujet.

C'est par la que ça se passe : [Le Ruby Nouveau](https://lerubynouveau.fr).

Bon appétit ! ;)
