---
title: Un nouveau look pour le blog
date: 2016-03-06 13:12:07 UTC
tags: blog
cover: book.jpg
---
Il y avait décidément trop d'images sur cette page d'accueil et cela faisait quelques temps que je souhaitais rendre plus lisible le texte des billets.

READMORE

C'est désormais chose faite avec ce nouveau thème : [Lanyon](http://lanyon.getpoole.com/) issu du monde "Jekyll" que je trouve particulièrement épuré.

J'ai hésité (et j'avoue hésiter encore) avec un autre thème épuré que j'apprécie également : [Casper](https://github.com/danielbayerlein/middleman-casper). C'est le thème de base utilisé par le CMS [Ghost](https://ghost.org) avec la même philosophie de simplicité, mais j'ai trouvé que Lanyon était plus facile à adapter à la logique existante de ma configuration de [Middleman 3](https://middlemanapp.com).

Eh non, je ne suis toujours pas passé à la version 4, le monde du CMS statique étant moins sensible à la montée en version des générateurs…
