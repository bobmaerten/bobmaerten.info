# Bob Maerten personal blog

This is the codebase of my personal blog. The blog is statically generated with [Middleman](https://middlemanapp.com/).

The published blog address is https://blog.bobmaerten.info
