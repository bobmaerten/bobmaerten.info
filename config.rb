# encoding: UTF-8

###
# middleman-casper configuration
###

config[:casper] = {
  blog: {
    url: 'https://blog.bobmaerten.info',
    name: 'Bob Maerten',
    description: 'Humeurs, interrogations, pâtisseries, développement web, systèmes Linux et autres curiosités.',
    date_format: '%e %B %Y',
    date_locale: 'fr',
    navigation: true,
    logo: nil # Optional
  },
  author: {
    name: 'Bob Maerten',
    bio: 'Sysadmin en rémission et en désintoxication de la fonction publique. Développeur web chez Level Up Solutions.',
    location: nil, # Optional
    website: nil, # Optional
    twitter: 'https://twitter.com/bobmaerten' # Optional
  },
  navigation: {
    "Accueil" => "/",
    "Maintenant" => "/now/",
    "Outils" => "/uses/",
    "Archives" => "/archives/",
    "Catégories" => "/tags/",
  }
}

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# With alternative layout
# page "/path/to/file.html", layout: :otherlayout

# Proxy pages (https://middlemanapp.com/advanced/dynamic_pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

def get_tags(resource)
  if resource.data.tags.is_a? String
    resource.data.tags.split(',').map(&:strip)
  else
    resource.data.tags
  end
end

def group_lookup(resource, sum)
  results = Array(get_tags(resource)).map(&:to_s).map(&:to_sym)

  results.each do |k|
    sum[k] ||= []
    sum[k] << resource
  end
end

tags = resources
  .select { |resource| resource.data.tags }
  .each_with_object({}, &method(:group_lookup))

tags.each do |tag, articles|
  proxy "/tag/#{tag.downcase.to_s.parameterize}/feed.xml", '/feed.xml', locals: { category: tag, articles: articles[0..5] }, layout: false
end
# proxy "/feed.xml", "/blog/feed.xml"
# proxy "/atom.xml", "/blog/feed.xml"
# proxy "/blog/atom.xml", "/blog/feed.xml"

proxy "/author/#{config.casper[:author][:name].parameterize}.html",
  '/author.html', ignore: true

activate :i18n, langs: :fr

# General configuration
# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

###
# Helpers
###

activate :blog do |blog|
  # This will add a prefix to all links, template references and source paths
  # blog.prefix = "blog"

  blog.permalink = "{title}.html"
  # Matcher for blog source files
  blog.sources = "blog/{year}-{month}-{day}-{title}.html"
  blog.taglink = "tag/{tag}.html"
  blog.layout = "post"
  blog.summary_separator = /READMORE/
  # blog.summary_length = 250
  # blog.year_link = "{year}.html"
  # blog.month_link = "{year}/{month}.html"
  # blog.day_link = "{year}/{month}/{day}.html"
  blog.default_extension = ".markdown"
  # blog.new_article_template = 'blog_post.tmpl'

  blog.tag_template = "tag.html"
  # blog.calendar_template = "calendar.html"

  # Enable pagination
  blog.paginate = true
  blog.per_page = 5
  blog.page_link = "page/{num}"
end

# Pretty URLs - https://middlemanapp.com/advanced/pretty_urls/
activate :directory_indexes

# Middleman-Syntax - https://github.com/middleman/middleman-syntax
set :haml, { ugly: true }
set :markdown_engine, :redcarpet
set :markdown, fenced_code_blocks: true, smartypants: true, footnotes: true,
  link_attributes: { rel: 'nofollow' }, tables: true, strikethrough: true
activate :syntax, line_numbers: false

# Middleman-Sprockets - https://github.com/middleman/middleman-sprockets
activate :sprockets

# Methods defined in the helpers block are available in templates
# helpers do
#   def some_helper
#     "Helping"
#   end
# end

# Build-specific configuration
configure :build do
  Tilt::SYMBOL_ARRAY_SORTABLE = false
  activate :favicon_maker do |f|
    f.template_dir  = File.join(root, 'source')
    f.output_dir    = File.join(root, 'build')
    f.icons = {
      '_favicon_base.png' => [
        { icon: 'chrome-touch-icon-192x192.png' },
        { icon: 'apple-touch-icon.png', size: '152x152' },
        { icon: 'ms-touch-icon-144x144-precomposed.png', size: '144x144' },
        { icon: "apple-touch-icon-180x180-precomposed.png" },
        { icon: "apple-touch-icon-152x152-precomposed.png" },
        { icon: "apple-touch-icon-144x144-precomposed.png" },
        { icon: "apple-touch-icon-120x120-precomposed.png" },
        { icon: "apple-touch-icon-114x114-precomposed.png" },
        { icon: "apple-touch-icon-76x76-precomposed.png" },
        { icon: "apple-touch-icon-72x72-precomposed.png" },
        { icon: "apple-touch-icon-60x60-precomposed.png" },
        { icon: "apple-touch-icon-57x57-precomposed.png" },
        { icon: "apple-touch-icon-precomposed.png", size: "57x57" },
        { icon: "apple-touch-icon.png", size: "57x57" },
        { icon: 'favicon-196x196.png' },
        { icon: 'favicon-160x160.png' },
        { icon: 'favicon-96x96.png' },
        { icon: 'favicon-32x32.png' },
        { icon: 'favicon-16x16.png' },
        { icon: 'favicon.png', size: '16x16' },
        { icon: 'favicon.ico', size: '64x64,48x48,32x32,24x24,16x16' }
      ]
    }
  end
  activate :minify_css
  activate :minify_javascript
  activate :asset_hash, ignore: %r{^slides\/.*}
  activate :relative_assets
  # activate :gzip

  # Ignoring Files
  ignore 'javascripts/_*'
  ignore 'javascripts/vendor/*'
  ignore 'stylesheets/_*'
  ignore 'stylesheets/vendor/*'
end

# # Deploying on S3
# activate :s3_sync do |s3_sync|
#   s3_sync.region                     = 'eu-west-1'
#   s3_sync.delete                     = true
#   s3_sync.after_build                = false # We do not chain after the build step by default.
#   s3_sync.prefer_gzip                = false
#   s3_sync.path_style                 = true
#   s3_sync.reduced_redundancy_storage = false
#   s3_sync.acl                        = 'public-read'
#   s3_sync.encryption                 = false
#   s3_sync.prefix                     = ''
#   s3_sync.version_bucket             = false
#   s3_sync.index_document             = 'index.html'
#   s3_sync.error_document             = '404.html'
# end

activate :deploy do |deploy|
  deploy.deploy_method = :rsync
  deploy.host   = 'syno'
  deploy.user   = 'bob'
  deploy.port   = '2222'
  deploy.path   = '/volume1/web/bobmaerten.info/blog'
  deploy.flags  = '-avz --rsync-path=/usr/bin/rsync' # add custom flags, default: -avz
  deploy.clean  = true
end
